﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalPointer : MonoBehaviour
{
    public Transform goal;


    void Update()
    {
        transform.LookAt(goal);
    }
}
