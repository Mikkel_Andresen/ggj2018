﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Torpedo Loot Piece", menuName = "Loot/Torpedo Loot Piece", order = 4)]
public class TorpedoLoot : LootPiece {
    public int maxTorpedos = 10;
    public int minTorpedos = 5;
    private int torpedos;

    public override void GiveLoot(GameObject uboat)
    {
        torpedos = Random.Range(minTorpedos, maxTorpedos);

        base.GiveLoot(uboat);

        uboat.GetComponent<UBoatTorpedos>().AddTorpedo(torpedos);
        Debug.Log("Received torpedos");
    }
}
