﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Fuel Loot Piece", menuName = "Loot/Fuel Loot Piece", order = 2)]
public class FuelLoot : LootPiece
{
    public int maxFuelAmount = 10;
    public int minFuelAmount = 5;
    private int fuel;

    public override void GiveLoot(GameObject uboat)
    {
        fuel = Random.Range(minFuelAmount, maxFuelAmount);

        base.GiveLoot(uboat);

        // Give fuel
        uboat.GetComponent<UBoatFuel>().AddFuel(fuel);

        Debug.Log("Give fuel");
    }
}
