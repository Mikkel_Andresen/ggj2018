﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Repair Loot Piece", menuName = "Loot/Repair Loot Piece", order = 3)]
public class RepairLoot : LootPiece {
    public int maxHealth = 10;
    public int minHealth = 5;
    private int health;

    public override void GiveLoot(GameObject uboat)
    {
        health = Random.Range(minHealth, maxHealth);

        base.GiveLoot(uboat);

        uboat.GetComponent<UBoatHealth>().AddHealth(health);
        Debug.Log("Received repairs");
    }
}
