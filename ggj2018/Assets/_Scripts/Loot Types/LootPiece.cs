﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Loot Piece", menuName = "Loot/Loot Piece", order = 10)]
public class LootPiece : ScriptableObject
{
    public ParticleSystem spawnParticles;

    public virtual void GiveLoot(GameObject uboat)
    {
        if (spawnParticles != null)
        {
            ParticleSystem particles = Instantiate(spawnParticles, uboat.transform.position, Quaternion.identity);
            particles.transform.SetParent(GameController.instance.mapParent);
        }
    }
}
