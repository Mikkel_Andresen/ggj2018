﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Mine Loot Piece", menuName = "Loot/Mine Loot Piece", order = 2)]
public class MineLoot : LootPiece
{
    public int maxDamage = 10;
    public int minDamage = 5;
    private int damage;
    
    public override void GiveLoot(GameObject uboat)
    {
        damage = Random.Range(minDamage, maxDamage);

        base.GiveLoot(uboat);

        uboat.GetComponent<UBoatHealth>().SubtractHealth(damage);
        Debug.Log("Hit mine");
    }
}
