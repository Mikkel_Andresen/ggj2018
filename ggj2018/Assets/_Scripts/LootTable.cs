﻿using UnityEngine;

[System.Serializable]
public struct LootTiers
{
    [Tooltip("Put prefabs for the loot piece and a prefab for the spawn particle of each particular piece")]
    public LootPiece[] highTierLoot;
    [Tooltip("Put prefabs for the loot piece and a prefab for the spawn particle of each particular piece")]
    public LootPiece[] mediumTierLoot;
    [Tooltip("Put prefabs for the loot piece and a prefab for the spawn particle of each particular piece")]
    public LootPiece[] lowTierLoot;

    public LootTiers(LootPiece[] highTierLoot, LootPiece[] mediumTierLoot, LootPiece[] lowTierLoot)
    {
        this.highTierLoot = highTierLoot;
        this.mediumTierLoot = mediumTierLoot;
        this.lowTierLoot = lowTierLoot;
    }
}

[CreateAssetMenu(fileName = "New Loot Table", menuName = "Loot/Loot Table", order = 1)]
public class LootTable : ScriptableObject
{
    public LootTiers lootTiers;
}
