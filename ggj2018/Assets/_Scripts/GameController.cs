﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public LootTable masterLootTable;
    public Transform mapMesh;
    public Transform mapParent;
    public UBoatController control;

    public MineLoot enemy;
    public LootPickup lootPrefab;
    public Canvas lootItemUIPiecesParent;
    public GameObject lootItemChanceUI;
    public Transform goal;
    public float goalMinDist = 100f;

    public int lootAmount = 1000;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(instance);
        }

        instance = this;
    }

    private void Start()
    {
        // Randomly spawn pieces of loot around the map with highest chance of spawning low tier loot
        float maxX = mapMesh.localScale.x * 5;
        float maxZ = mapMesh.localScale.z * 5;

        // Set goal position
        //float realMinDist = Vector3.Distance(new Vector3(maxX, 0, maxZ), new Vector3(-maxX, 0, -maxZ));
        //if (goalMinDist > realMinDist)
        //{
        //    goalMinDist = realMinDist;
        //}

        Transform player = GameObject.FindWithTag("Player").transform;
        Vector3 goalPos = Vector3.zero;
        float dist = Vector3.Distance(goalPos, player.position);

        while (dist < goalMinDist)
        {
            goalPos = new Vector3(Random.Range(-maxX, maxX), 0, Random.Range(-maxZ, maxZ));
            dist = Vector3.Distance(goalPos, player.position);
        }

        Debug.Log("Distance to goal: " + dist.ToString());
        goal.position = new Vector3(goalPos.x, goal.position.y, goalPos.z);

        // Spawn loot
        for (int i = 0; i < lootAmount; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-maxX, maxX), 0, Random.Range(-maxZ, maxZ));

            LootPickup lootPiece = Instantiate(lootPrefab, pos, lootPrefab.transform.rotation);
            lootPiece.transform.SetParent(mapParent);
            float random = Random.value;
            lootPiece.Setup(random);
            Text t = lootPiece.GetComponentInChildren<Text>(true);
            t.text = (random * 100).ToString("0.0") + "%";
            Image img = lootPiece.GetComponentInChildren<Image>(true);

            if (random >= 0.8f) // High tier
            {
                img.color = Color.red;
            }
            else if (random >= 0.5f && random < 0.8f) // Medium tier
            {
                img.color = Color.yellow;
            }
            else if (random >= 0.3f && random < 0.5f) // Low tier
            {
                img.color = Color.green;
            }
            else if (random < 0.3f) // Low tier
            {
                img.color = Color.green;
            }
        }
    }
}
