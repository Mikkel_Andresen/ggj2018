﻿using UnityEngine;

public class LootGiver : MonoBehaviour, ICanLoot
{
    void ICanLoot.GiveLoot(LootPiece loot)
    {
        if (loot != null)
        {
            loot.GiveLoot(gameObject);
        }
    }
}
