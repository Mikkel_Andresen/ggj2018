﻿using System.Collections;
using UnityEngine;

public class ParticleDisable : MonoBehaviour
{
    private ParticleSystem sys;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(ParticleTimer(sys, sys.main.duration));
    }

    IEnumerator ParticleTimer(ParticleSystem sys, float duration)
    {
        yield return new WaitForSeconds(duration);
        sys.gameObject.SetActive(false);
    }
}
