﻿using UnityEngine;
using System.Collections;

public class UBoatHealth : MonoBehaviour
{
    [SerializeField]
    Animator anim;
    [SerializeField]
    float animSpeed = 1;

    public float curHealth;
    public float maxHealth = 100;
    private UBoatSurvivial survivialSystem;

    private void Start()
    {
        survivialSystem = GetComponent<UBoatSurvivial>();

        if (anim != null)
        {
            float startTime = curHealth / maxHealth;
            gaugeAnimation = StartCoroutine(GaugeAnimation(startTime, curHealth / maxHealth));
        }
    }


#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            AddHealth(10);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            SubtractHealth(10);
        }
    }
#endif

    public void AddHealth(float amount)
    {
        float newHealth = curHealth + amount;

        if (newHealth > maxHealth)
            return;

        float startTime = curHealth / maxHealth;
        curHealth = newHealth;

        if (anim != null)
        {
            //anim.Play("GaugeMove", 0, curHealth / maxHealth);
            if (gaugeAnimation != null)
                StopCoroutine(gaugeAnimation);

            gaugeAnimation = StartCoroutine(GaugeAnimation(startTime, curHealth / maxHealth));
        }
    }

    public void SubtractHealth(float amount)
    {
        float newHealth = curHealth - amount;
        if (newHealth <= 0)
        {
            // Call lose by death
            curHealth = 0;
            Lose();

            return;
        }

        float startTime = curHealth / maxHealth;
        curHealth = newHealth;

        if (anim != null)
        {
            //anim.Play("GaugeMove", 0, curHealth / maxHealth);
            if (gaugeAnimation != null)
                StopCoroutine(gaugeAnimation);

            gaugeAnimation = StartCoroutine(GaugeAnimation(startTime, curHealth / maxHealth, true));
        }
    }

    private Coroutine gaugeAnimation;
    IEnumerator GaugeAnimation(float startTime, float targetTime, bool subtract = false)
    {
        float t = startTime;
        while (true)
        {
            t = Mathf.Lerp(t, targetTime, Time.deltaTime * animSpeed);
            anim.Play("HealthGaugeMove", 0, t);

            if ((t >= targetTime && !subtract) || (t <= targetTime && subtract))
                break;
            else
                yield return null;
        }
    }

    private void Lose()
    {
        survivialSystem.Lost(UBoatSurvivial.LoseReason.Death);
    }
}
