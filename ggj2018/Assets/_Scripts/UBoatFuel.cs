﻿using UnityEngine;
using System.Collections;
using System;

public class UBoatFuel : MonoBehaviour
{
    [SerializeField]
    Animator anim;
    [SerializeField]
    float animSpeed = 1;

    public float curFuel;
    public float maxFuel = 100;
    private UBoatSurvivial survivialSystem;

    public event Action OnOutOfFuel;

    private void Start()
    {
        survivialSystem = GetComponent<UBoatSurvivial>();

        if (anim != null)
        {
            float startTime = curFuel / maxFuel;
            gaugeAnimation = StartCoroutine(GaugeAnimation(startTime, curFuel / maxFuel));
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            AddFuel(10);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            SubtractFuel(10);
        }
    }
#endif

    public void AddFuel(float amount)
    {
        float newFuel = curFuel + amount;

        if (newFuel >= maxFuel)
            return;

        float startTime = curFuel / maxFuel;
        curFuel = newFuel;

        if (anim != null)
        {
            if (gaugeAnimation != null)
                StopCoroutine(gaugeAnimation);

            gaugeAnimation = StartCoroutine(GaugeAnimation(startTime, curFuel / maxFuel));
        }
    }

    public void SubtractFuel(float amount)
    {
        float newFuel = curFuel - amount;
        if (newFuel <= 0)
        {
            // Call lose by lack of fuel
            curFuel = 0;
            Lose();
            return;
        }

        float startTime = curFuel / maxFuel;
        curFuel = newFuel;

        if (anim != null)
        {
            if (gaugeAnimation != null)
                StopCoroutine(gaugeAnimation);

            gaugeAnimation = StartCoroutine(GaugeAnimation(startTime, curFuel / maxFuel, true));
        }
    }

    private Coroutine gaugeAnimation;
    IEnumerator GaugeAnimation(float startTime, float targetTime, bool subtract = false)
    {
        float t = startTime;
        while (true)
        {
            t = Mathf.Lerp(t, targetTime, Time.deltaTime * animSpeed);
            anim.Play("FuelGaugeMove", 0, t);

            if ((t >= targetTime && !subtract) || (t <= targetTime && subtract))
                break;
            else
                yield return null;
        }
    }

    private void Lose()
    {
        if (OnOutOfFuel != null)
            OnOutOfFuel.Invoke();

        survivialSystem.Lost(UBoatSurvivial.LoseReason.LowFuel);
    }
}
