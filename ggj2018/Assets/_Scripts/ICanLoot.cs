﻿using UnityEngine;

public interface ICanLoot
{
    void GiveLoot(LootPiece loot);
}
