﻿using UnityEngine;

public class LootPickup : MonoBehaviour
{
    public LootPiece actualLootPiece;
    public LootPiece potentialLootPiece;

    public void Setup(float lootTier)
    {
        // Determine Loot and spawn chance UI
        // Also chance color of UI depending on chance, i.e 80% = red and 0% = green
        LootTable table = GameController.instance.masterLootTable;

        if (lootTier >= 0.8f)
        {
            int val = Random.Range(0, table.lootTiers.highTierLoot.Length);
            potentialLootPiece = table.lootTiers.highTierLoot[val];

            if (Random.value > 0.8f) // Give either enemy or high tier loot
            {
                actualLootPiece = potentialLootPiece;
            }
            else
            {
                actualLootPiece = GameController.instance.enemy;
            }
        }
        else if (lootTier >= 0.5f && lootTier < 0.8f)
        {
            int val = Random.Range(0, table.lootTiers.mediumTierLoot.Length);
            potentialLootPiece = table.lootTiers.mediumTierLoot[Random.Range(0, table.lootTiers.highTierLoot.Length)];

            if (Random.value > 0.5f) // Give either enemy or medium tier loot
            {
                actualLootPiece = potentialLootPiece;
            }
            else
            {
                actualLootPiece = GameController.instance.enemy;
            }
        }
        else if (lootTier >= 0.3f && lootTier < 0.5f)
        {
            int val = Random.Range(0, table.lootTiers.lowTierLoot.Length);
            actualLootPiece = table.lootTiers.lowTierLoot[Random.Range(0, table.lootTiers.highTierLoot.Length)];

            if (Random.value > 0.3f) // Give either enemy or low tier loot
            {
                actualLootPiece = potentialLootPiece;
            }
            else
            {
                actualLootPiece = GameController.instance.enemy;
            }
        }
        else if (lootTier < 0.3f)
        {
            int val = Random.Range(0, table.lootTiers.lowTierLoot.Length);
            actualLootPiece = table.lootTiers.lowTierLoot[Random.Range(0, table.lootTiers.highTierLoot.Length)];

            if (Random.value > 0.1f) // Give either enemy or low tier loot
            {
                actualLootPiece = potentialLootPiece;
            }
            else
            {
                actualLootPiece = GameController.instance.enemy;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Give loot on trigger
        if (other.CompareTag("Player") || other.CompareTag("AI"))
        {
            UBoatTorpedos torpedos = other.GetComponent<UBoatTorpedos>();
            if (torpedos != null && torpedos.curTorpedoCount > -1)
            {
                // Give the potential loot
                torpedos.SubtractTorpedo(1);
                other.GetComponent<ICanLoot>().GiveLoot(potentialLootPiece);
            }
            else
            {
                // Give actual loot
                other.GetComponent<ICanLoot>().GiveLoot(actualLootPiece);
            }
            gameObject.SetActive(false);
        }
    }
}
