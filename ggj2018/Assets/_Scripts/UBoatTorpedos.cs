﻿using UnityEngine;

public class UBoatTorpedos : MonoBehaviour
{
    private int maxTorpedoCount = 5;
    private int minTorpedoCount = 0;
    public int curTorpedoCount = 1;

    public GameObject[] torpedoVisuals;
    private void Start()
    {
        maxTorpedoCount = torpedoVisuals.Length;

        for (int i = 0; i < maxTorpedoCount; i++)
        {
            if (i > curTorpedoCount)
                torpedoVisuals[i].SetActive(false);
            else
                torpedoVisuals[i].SetActive(true);
        }
    }
#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            AddTorpedo(1);
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            SubtractTorpedo(1);
        }
    }
#endif

    public void AddTorpedo(int amount)
    {
        int newTorpedoCount = curTorpedoCount + amount;
        if (newTorpedoCount > maxTorpedoCount - 1)
        {
            return;
        }
        curTorpedoCount = newTorpedoCount;

        torpedoVisuals[curTorpedoCount].SetActive(true);
    }

    public void SubtractTorpedo(int amount)
    {
        int newTorpedoCount = curTorpedoCount - amount;
        if (newTorpedoCount < -1)
        {
            return;
        }

        torpedoVisuals[curTorpedoCount].SetActive(false);
        curTorpedoCount = newTorpedoCount;
    }
}
