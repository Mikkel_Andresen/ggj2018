﻿using UnityEngine;

public class UBoatController : UBoatBase
{
    public float camHeight = 10;
    [SerializeField]
    Transform player;

    [SerializeField]
    LayerMask layers;

    [SerializeField]
    float fuelConsumption = 1;

    private Vector3 clickPos;
    private Vector3 targetPos = Vector3.zero;

    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;
    private Camera cam;

    private UBoatHealth health;
    private UBoatFuel fuel;

    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        health = player.GetComponent<UBoatHealth>();
        fuel = player.GetComponent<UBoatFuel>();
    }
    
    // Update is called once per frame
    void Update()
    {
        // Find the direction, find the length and then find the position at the end of that length
        Ray ray = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
        RaycastHit hit;
        Vector3 dir = Vector3.zero;

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layers))
            {
                clickPos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                startTime = Time.time;

                // Get normalized player direction
                dir = (player.position - clickPos).normalized;

                // Get the distance
                journeyLength = Vector3.Distance(clickPos, player.position);

                // Get the target positions from the length and offset by this position
                targetPos = (dir * journeyLength) + transform.position;

                Debug.DrawLine(transform.position, targetPos, Color.blue, 10f);
            }
        }

        if (targetPos != transform.position && targetPos != Vector3.zero)
        {
            float distCovered = (Time.time - startTime) * speed;
            float fracJourney = distCovered / journeyLength;
            transform.position = Vector3.Lerp(transform.position, targetPos, fracJourney);

            // Lower fuel each tick
            fuel.SubtractFuel(Time.deltaTime * fuelConsumption);
        }
    }
}
