﻿using UnityEngine;

public class UBoatSurvivial : MonoBehaviour
{
    [SerializeField]
    GameObject endScreen, outOfFuel, died, reachedGoal;

    public enum LoseReason
    {
        Death,
        LowFuel
    }

    public enum WinReason
    {
        OutOfOpponents,
        ReachedGoal
    }

    public void Lost(LoseReason reason)
    {
        if (endScreen != null)
            endScreen.SetActive(true);

        // Turn off controls
        GameController.instance.control.enabled = false;

        switch (reason)
        {
            case LoseReason.Death:
                if (died != null)
                    died.SetActive(true);
                break;
            case LoseReason.LowFuel:
                if (outOfFuel != null)
                    outOfFuel.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void Won(WinReason reason)
    {
        if (endScreen != null)
            endScreen.SetActive(true);

        // Turn off controls
        GameController.instance.control.enabled = false;

        switch (reason)
        {
            case WinReason.OutOfOpponents:
                break;
            case WinReason.ReachedGoal:
                if (reachedGoal != null)
                    reachedGoal.SetActive(true);
                break;
            default:
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Goal"))
        {
            if (endScreen != null)
                endScreen.SetActive(true);

            Won(WinReason.ReachedGoal);
        }
    }
}
